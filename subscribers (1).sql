-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2020 at 05:50 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `universalladder`
--

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `name`, `email`, `phone`, `country`, `status`, `created_at`, `updated_at`) VALUES
(1, 'vebyw@mailinator.net', 'wejumy@mailinator.com', '+1 (597) 828-5722', NULL, 1, '2019-05-05 02:45:23', '2019-05-05 02:45:23'),
(2, 'jeguzobyk@mailinator.net', 'cyfydacoz@mailinator.com', '+1 (118) 897-1351', NULL, 1, '2019-05-16 04:17:09', '2019-05-16 04:17:09'),
(3, 'jevup@mailinator.com', 'hugibisuw@mailinator.com', '+1 (681) 354-9123', NULL, 1, '2019-05-16 04:19:12', '2019-05-16 04:19:12'),
(5, 'Roshan Kunwar', 'roshan1@gmail.com', NULL, NULL, 1, '2020-02-19 08:45:48', '2020-02-19 08:45:48'),
(6, 'Roshan Kunwar', 'roshan.kunwar110@gmail.com', NULL, NULL, 1, '2020-02-19 08:52:58', '2020-02-19 09:34:49'),
(7, 'Roshan', 'roshan@gmail.com', NULL, NULL, 1, '2020-02-19 09:37:01', '2020-02-19 09:37:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
