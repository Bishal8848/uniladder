<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counselor extends Model
{
    protected $table='counselors';
    protected $fillable=['name','email','phone','details','status'];
}
