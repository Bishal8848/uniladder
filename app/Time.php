<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $table='times';
    protected $fillable=['counselor_id','time','status'];
    protected $attributes = [
        'status' => 1,
    ];
}
