<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    protected $atrributes=[
        'status'=> 0,
    ];

    protected $fillable=['name','address','email','phone','country_id','service_title','message','status'];

    public function country()
    {
        return $this->belongsTo('App\Country','country_id');
    }
}
