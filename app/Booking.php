<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $attributes = [
        'status' => 0,
    ];
    protected $table='bookings';
    protected $fillable=['counselor_id','name','email','phone','address','date','time'];
}
