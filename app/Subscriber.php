<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
	protected $attributes = [
	'status' => 1,
	];
	protected $fillable = [
		'name', 'email', 'status',
	];
	public function scopeActive($query)
	{
		return $query->where('status', 1);
	}
}
