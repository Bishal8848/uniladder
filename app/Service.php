<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
    	'title', 'details', 'image','icon',
    ];
    public static $_thumbdir = [
        '700x525',
        '400x300',
        '100x80',
        '300x300',
    ];
    protected $attributes = [
        'status' => 1,
    ];

    protected $maxfileSize = 2*1024*1024;

    public function companies()
    {
    	return $this->hasMany(UserCompany::class)->where('status', 1);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
    public function thumb($size = '300x300')
    {
        if($this->image){
            $image = public_path('uploads/services/' . $this->image);
            if (file_exists($image)) {
                return url('upload/services/' . $size . '/' . $this->image);
            }else{
                return 'holder.js/200x200/image not found';
            }
        }else{
            return 'holder.js/200x200/image not found';
        }
    }

    public function image($size = null)
    {

        $_image = false;
        if($size){

            $original = public_path('uploads/services/'. $this->image);
            if($this->image && file_exists($original)){
                $_image = asset('uploads/services/' . $size . '/' . $this->image);
                $_image_path = public_path('uploads/services/' . $size . '/' . $this->image);
                if(!file_exists($_image_path)){
                    $_image = new  _image('services', $size, $this->image);
                }
            }
        }else{
            $original = public_path('uploads/services/'. $this->image);
            if($this->image && file_exists($original)){
                $_image = asset('uploads/services/'. $this->image);
            }
        }
        return $_image;
    }
}
