<?php

namespace App\Mail;

use App\Subscriber;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ServicesInfo extends Mailable
{
    use Queueable, SerializesModels;
    public $subscriber;
    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request,$subscriber)
    {
        $this->request = $request;
//        $subscriber=Subscriber::all();
        $this->subscriber = $subscriber;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Service Info:: ' .config('app.name'))->markdown('emails.services.serviceinfo');
    }
}
