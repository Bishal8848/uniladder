<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppointmentApproved extends Mailable
{
    use Queueable, SerializesModels;
    public $booking;
    public $id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($booking,$id)
    {
        $this->booking = $booking;
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->booking->email)->markdown('emails.appointment.approved');
    }
}
