<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppointmentCounselorApproved extends Mailable
{
    use Queueable, SerializesModels;
    public $booking;
    public $id;
    public $counselor;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($booking,$id)
    {
        $this->booking = $booking;
        $this->counselor =\App\Counselor::where('id', $this->booking['counselor_id'])->first();
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->counselor->email)->markdown('emails.appointment.counselorapproved');
    }
}
