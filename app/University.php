<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $table='universities';
    protected $fillable=['country','location','uname','details','website','image','services','status'];
}
