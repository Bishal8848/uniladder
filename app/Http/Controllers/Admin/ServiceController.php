<?php

namespace App\Http\Controllers\Admin;

use App\Mail\ServicesInfo;
use App\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use Mail;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        return view('admin.service.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $data = array(
            'title' => $request->title,
            'details' => $request->details,
            'icon' => $request->icon,
        );

        $service = Service::create($data);

        if($request->hasFile('image'))
        {
            $image_name = $service->title.'-'.$service->id.'.'.$request->image->extension();
            $path = $request->image->move('uploads/services/',$image_name);
            $service->image = $image_name;
            $service->save();
        }

        $subscribers=Subscriber::all();
//        dd($subscriber->email);
        foreach($subscribers as $subscriber)
        {
//            dd($subscriber);
            Mail::to($subscriber->email)->send(new ServicesInfo($request,$subscriber));
        }

        return redirect()->route('services.index')->with('success','Service Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin.service.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $data = array(
            'title' => $request->title,
            'details' => $request->details,
            'icon' =>$request->icon,
        );
        if($request->hasFile('image'))
        {
            if($service->image && file_exists($image_path = public_path('uploads/services/' . $service->image))){
                unlink($image_path);
            }

            $image_name = $service->title.'-'.$service->id.'.'.$request->image->extension();
            $path = $request->image->move('uploads/services/',$image_name);
            $service->image = $image_name;
            $service->save();
        }
//        dd($data);

        if(Service::where('id',$service->id)->update($data))
        {
            $subscribers=Subscriber::active()->get();
            foreach($subscribers as $subscriber)
            {
                Mail::to($subscriber->email)->send(new ServicesInfo($data, $subscriber));
            }

            return redirect()->route('services.index')->with('success','Service added.');
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        if($service){

            if($service->image){
                $image = public_path('uploads/services/' . $service->image);
                if(file_exists($image)){
                    unlink($image);
                }
            }

            if($service->delete()){
                return redirect()->route('services.index')->with('success', 'service deleted.');
            }else{
                return redirect()->route('services.index')->with('error', 'Error while deleting service.');
            }

        }else{
            abort();
        }
    }
}
