<?php

namespace App\Http\Controllers\Admin;
use App\University;
use App\Country;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UniversityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $universities = University::all();
        return view('admin.universities.index', compact('universitiess'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['countries'] = Country::all();
        $data['services'] = Page::where('parent_id', 8)->orderBy('order_by')->get();
        return view('admin.universities.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//         dd($request);
        $this->validate($request, [
            'country' => 'required',
            'location' => 'required',
            'university' => 'required',
            'details'=> 'required',
        ]);
        $universities = new University();
        $universities->country = $request->country;
        $universities->location = $request->location;
        $universities->uname = $request->university;
        // $universities->services = json_encode($request->services);
        $universities->details = $request->details;
        $universities->website=$request->website;
        $universities->status=0;
        $universities->save();
        if ($request->hasFile('image')) {
            $image_name = $request->university. '-'.$universities->id. '.'. $request->image->extension();
            $path = $request->image->move('uploads/university/', $image_name);
            $universities->image = $image_name;
            $universities->save();
        }


        return redirect()->route('universities.index')->with('success', 'University added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function universities()
    {

    }
}
