<?php

namespace App\Http\Controllers\Admin;

use App\Counselor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CounselorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counselor = Counselor::all();
        return view('admin.counselor.index',compact('counselor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.counselor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $counselor = new Counselor();
        $counselor->name = $request->name;
        $counselor->email=$request->email;
        $counselor->phone=$request->phone;
        $counselor->details = $request->details;
        $counselor->status=0;
        $counselor->save();
        return redirect()->route('counselor.index')->with('success', 'Counselor added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Counselor $counselor)
    {
        return view('admin.counselor.edit',compact('counselor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Counselor $counselor)
    {
        $this->validate($request, [
            'name' => 'required',
            'email'=>'email',
            'phone'=>'phone',
        ]);

        $data = array(
            'name' => $request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'details' => $request->details,
        );
        $counselor=Counselor::where('id', $counselor->id)->update($data);
        return redirect()->route('counselor.index')->with('success','Counselor Edited successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Counselor $counselor)
    {
        if($counselor->delete()){
            return redirect()->route('counselor.index')->with('success', 'counselor deleted.');
        }else{
            return redirect()->route('counselor.index')->with('error', 'Error while deleting counselor.');
        }
    }
}
