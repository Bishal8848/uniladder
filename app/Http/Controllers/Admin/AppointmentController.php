<?php

namespace App\Http\Controllers\Admin;
use App\Booking;
use App\Mail\AppointmentApproved;
use App\Mail\AppointmentCounselorApproved;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

class AppointmentController extends Controller
{
   public function reviewbooking()
   {
       $bookings= Booking::all();
       return view('admin.booking.index',compact('bookings'));
   }

    public function decline($id)
    {
        $booking = Booking::find(decrypt($id));

        $data = array(
            'status' => -1,

        );
        if(Booking::where('id',$booking->id)->delete())
        {
            return redirect()->route('booking.review')->with('success','Appointment was declined');
        }
    }

    public function approve($id)
    {
        $booking = Booking::find(decrypt($id));




//        $issue->remaining=$value-1;
        $data = array(
            'status' => 1,

        );



        if(Booking::where('id',$booking->id)->update($data))
        {

            Mail::send(new AppointmentApproved($booking, $id));
            Mail::send(new AppointmentCounselorApproved($booking, $id));
            return redirect()->route('booking.review')->with('success','Appointment was approved');
        }
    }

}
