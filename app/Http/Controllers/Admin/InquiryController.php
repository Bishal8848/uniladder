<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\Inquiry;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inquiryinfos = Inquiry::all();
        return view('admin.inquiryinfo.index', compact('inquiryinfos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['countries'] = Country::all();
        $data['services'] = Page::where('parent_id', 8)->orderBy('order_by')->get();
        return view('admin.inquiryinfo.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'message'=>'required',
            'country'=>'required',
            'service'=>'required',
        ]);

        $inquiryinfo = new Inquiry();
        $inquiryinfo->name = $request->name;
        $inquiryinfo->email = $request->email;
        $inquiryinfo->address = $request->address;
        $inquiryinfo->phone = $request ->phone;
        $inquiryinfo->country=$request->country;
        $inquiryinfo->service=$request->service;
        $inquiryinfo->message=$request->message;
        $inquiryinfo->status=0;
        $inquiryinfo->save();

        return redirect()->route('inquiryinfos.index')->with('success', 'Inquiry Info added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Inquiry $inquiryinfo)
    {

        return view('admin.inquiryinfo.view', compact('inquiryinfo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Inquiry $inquiryinfo)
    {
        $data['countries'] = Country::all();
        $data['services'] = Page::where('parent_id', 8)->orderBy('order_by')->get();
        return view('admin.inquiryinfo.edit', $data,compact('inquiryinfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Inquiry $inquiryinfo)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'message'=>'required',
            'country'=>'required',
            'service'=>'required',
        ]);

        $inquiryinfo->name = $request->name;
        $inquiryinfo->email = $request->email;
        $inquiryinfo->address = $request->address;
        $inquiryinfo->phone = $request ->phone;
        $inquiryinfo->country_id=$request->country;
        $inquiryinfo->service_title=$request->service;
        $inquiryinfo->message=$request->message;
        $inquiryinfo->status=0;
        $inquiryinfo->save();

        return redirect()->route('inquiryinfos.index')->with('success', 'Inquiry Info added.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inquiry $inquiryinfo)
    {
        if($inquiryinfo && $inquiryinfo->delete()){
            return redirect()->route('inquiryinfos.index')->with('success', 'Inquiry Info deleted.');
        }else{
            return redirect()->route('inquiryinfos.index')->with('error', 'Error while deleting Inquiry Info.');
        }
    }
}
