<?php

namespace App\Http\Controllers\Admin;

use App\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptions=Subscriber::all();
        return view('admin.subscription.index',compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subscription.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $subscriber = Subscriber::where('email', $request->email)->first();
        if(!$subscriber){
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
            ]);

            $data = array(
                'name' => $request->name,
                'email' => $request->email,
                'status' => $request->status,
            );

            $subscription = Subscriber::create($data);
            return redirect()->route('subscriptions.index')->with('success','Subscriber Added.');

        }
        else
        {
            return redirect()->route('subscriptions.index')->with('error','Error while adding Subscriber.');
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscriber $subscription)
    {
        return view('admin.subscription.edit',compact('subscription'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscriber $subscription)
    {

        if ($subscription) {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
            ]);

            $data = array(
                'name' => $request->name,
                'email' => $request->email,
                'status' => $request->status,
            );

            $subscriber=Subscriber::where('id', $subscription->id)->update($data);
            return redirect()->route('subscriptions.index')->with('success','Subscriber Edited successfully.');

        }
        else
        {
            return redirect()->route('subscriptions.index')->with('error','Error while updating Subscriber.');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscriber $subscription)
    {
        if ($subscription) {
            if ($subscription->delete()) {
                return redirect()->route('subscriptions.index')->with('success', 'subscriber deleted.');
            } else {
                return redirect()->route('subscriptions.index')->with('error', 'Error while deleting subscriber.');
            }

        } else {
            abort();
        }
    }
}
