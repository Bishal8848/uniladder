<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscriber;
use App\User;

class SubscriptionController extends Controller
{
    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'captcha' => 'required',
        ]);
        if(\Session::get('rand_pass') <> $request->captcha){
            return redirect()->back()->withErrors(['captcha' => 'Invalid Cpatcha'])->withInput();
        }

        $subscriber = Subscriber::where('email', $request->email)->first();
        if(!$subscriber){
            $subscriber = new Subscriber;
            $subscriber->email = $request->email;
            $subscriber->name = $request->name;
            $subscriber->save();
        }
        else{
            return redirect()->route('_root_')->with('error', 'Email address already subscribed.');
        }

//        Mail::send(new RequestCallback($request));
        return redirect()->route('thankyou');
    }
    public function unsubscribe(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'captcha' => 'required',
        ]);
        if(\Session::get('rand_pass') <> $request->captcha){
            return redirect()->back()->withErrors(['captcha' => 'Invalid Cpatcha'])->withInput();
        }
        $subscriber = Subscriber::where('email', $request->email)->first();
        if($subscriber){
          $subscriber->delete();
          return redirect()->back();
        }
    }
}
