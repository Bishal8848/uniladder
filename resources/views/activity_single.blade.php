@extends('layouts.app')
@section('title')
    {{ $activities->title }}
@stop
@section('content')
    <div class="container">
        <div class="breadcrumbs-w3l">
        <span class="breadcrumbs">
            <a href="{{ route('_root_') }}">Home</a> |

            <span>{{ucfirst($activities->title)}}</span>
        </span>
        </div>
    </div>

    <div class="welcome-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading-text">
                        {{ $activities->title }}
                    </h2>
                    @if($activities->image)
                        <img src="{{asset('uploads/events/'.$activities->image)}}" alt="{{$activities->title}}" width="500" class="img-responsive pull-right" style="margin-left: 20px;">
                    @endif
                    {!! $activities->details !!}<br>
                    Venue: <h4>{{$activities->event_place}}</h4><br>
                    Date: <h4>{{$activities->event_date}}</h4><br>
                    Guest Speaker: <h4>{{$activities->lecturer}}</h4><br>
                    Hosted By: <h4>Uniladder Consultancy</h4>
                </div>
            </div>



        </div>
    </div>


@stop
@section('script')
@stop
