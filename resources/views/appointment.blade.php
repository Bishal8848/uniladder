@extends('layouts.app')
@section('title', 'Make Appointment ' . config('app.name'))
@section('page_title', 'Appointment')

@section('content')
    <div class="welcome-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading-text">Book Appointment</h2>
                    <form method="POST" action="{{route('saveappointments')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <!-- Form Group -->
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-sx-12">
                                    <!-- Element -->
                                    <div class="element">
                                        <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Name *" required>
                                    </div>
                                    <!-- End Element -->
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-sx-12">
                                    <!-- Element -->
                                    <div class="element">
                                        <input type="email" value="{{ old('email') }}" name="email" class="form-control" placeholder="E-mail" required="">
                                    </div>
                                    <!-- End Element -->
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-sx-12">
                                    <!-- Element -->
                                    <div class="element">
                                        <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" placeholder="Phone Number *" >
                                    </div>
                                    <!-- End Element -->
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <!-- Element -->
                                        <div class="element">
                                            <div>
                                                <!-- Element -->
                                                <div class="element">
                                                    <input type="text" class="form-control" placeholder="Your Address *" name="address">
                                                </div>
                                                <!-- End Element -->
                                            </div>
                                        </div>
                                        <!-- End Element -->
                                    </div>
                                    <!-- End form Group -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <!-- Element -->
                                        <div class="element">
                                            <div>
                                                <!-- Element -->
                                                <div class="element">
                                                    <input type="date" class="form-control"  name="date" id="date">
                                                </div>
                                                <!-- End Element -->
                                            </div>
                                        </div>
                                        <!-- End Element -->
                                    </div>
                                    <!-- End form Group -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-sx-12">
                                    <!-- Element -->
                                    <select name="counselor_id" id="counselor" class="form-control dynamic" data-dependent="time">
                                        <option value="">---- SELECT COUNSELOR----</option>
                                        @foreach ($counselor as $counselors)
                                            <option value="{{ $counselors->id }}">{{ $counselors->name }}</option>
                                        @endforeach
                                    </select>
                                    <!-- End Element -->
                                </div>
                            </div>
                        </div>

                        <!-- End form group -->


                        <div class="row">
                            <div class="col-md-4">
                                <!-- Form Group -->
                                <div class="form-group">
                                    <!-- Element -->
                                    <select name="time" id="time" class="form-control dynamic"></select>
                                    <!-- End Element -->
                                </div>
                                <!-- End form Group -->
                            </div>
                        </div>



                        <!-- Element -->
                        <br class="hidden-xs">
                        <div class="element">
                            <button type="submit" id="submit" value="Send" class="btn btn-black">Book</button>
                            <!-- <div class="loading"></div> -->
                        </div>
                        <!-- End Element -->
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){

        $('.dynamic').change(function(){
            if($(this).val() != '')
            {
                var select = $(this).attr("id");
                var value = $(this).val();
                var dependent = $(this).data('dependent');
                let date_selected=$('input[name="date"]').val();
                var _token = $('input[name="_token"]').val();
                console.log(select);
                console.log(value);
                console.log(dependent);
                $.ajax({
                    url:"{{ route('dynamindependent.fetch_time') }}",
                    method:"POST",
                    data:{select:select, value:value, _token:_token, dependent:dependent, date_selected:date_selected},
                    success:function(result)
                    {

                        $('#'+dependent).html(result);
                    }

                })
            }
        });

        $('#counselor').change(function(){
            $('#time').val('');
        });



    });
</script>
    @endsection
