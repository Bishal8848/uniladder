@extends('layouts.app')
@section('title')
    {{ $page->title }}
@stop
@section('content')
    <div class="container">
        <div class="breadcrumbs-w3l">
        <span class="breadcrumbs">
            <a href="{{ route('_root_') }}">Home</a> |
            @if($page->parent)
                <a href="{{ $page->parent->url }}">{{ $page->parent->title }}</a> |
            @endif
            <span>{{ $page->title }}</span>

        </span>
        </div>
    </div>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                @if(count($activities) >0 )
                    @foreach($activities as $activity)
                            <div class="card" style="margin-bottom: 20px;">
                                @if($activity->image)
                                    <img class="card-img-top" style="width: 560px; height: 260px"  src="{{url('uploads/events/'.$activity->image)}}" alt="{{$activity->title}}"><br>
                                @endif
                                <div class="card-body"  style="text-align: justify; text-justify: inter-word;">
                                    <h2 class="card-title"> {{$activity->title}}</h2><br>
                                    <p class="card-text"> {!! $activity->details !!}</p><br>
                                    <h5>Venue: {{$activity->event_place}}</h5><br>
                                    <h5>Date: {{$activity->event_date->format('M d, Y')}}</h5><br>
                                    <a href="{{route('view.activities',$activity->id)}}" class="btn btn-primary">View</a>
                                </div>
                            </div>

                    @endforeach
                @else
                    <h2 class="heading-text">
                        No Data Available
                    </h2>
                @endif
            </div>
        </div>
    </div>
</div>
{{--    <div class="welcome-wrap">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                    @if(count($activities) >0 )--}}
{{--                    @foreach($activities as $activity)--}}
{{--                        <div class="col-md-4">--}}
{{--                    <div class="card">--}}
{{--                        @if($activity->image)--}}
{{--                            <img class="card-img-top" style="" src="{{url('uploads/events/'.$activity->image)}}" alt="{{$activity->title}}">--}}
{{--                        @endif--}}
{{--                        <div class="card-body">--}}
{{--                            <h5 class="card-title">Title: {{$activity->title}}</h5>--}}
{{--                            <p class="card-text">Description: {!! $activity->details !!}</p>--}}
{{--                            <h5>VEnue: {{$activity->event_place}}</h5>--}}
{{--                            <h5>Date: {{$activity->event_date->format('M d, Y')}}</h5>--}}
{{--                            <a href="{{route('view.activities',$activity->id)}}" class="btn btn-primary">View</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                    @else--}}
{{--                        <h2 class="heading-text">--}}
{{--                            No Data Available--}}
{{--                        </h2>--}}
{{--                    @endif--}}





{{--            </div>--}}


{{--        </div>--}}
{{--    </div>--}}


@stop
@section('script')
@stop
