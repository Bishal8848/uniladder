@extends('layouts.app') 
@section('title') {{ $page->m_title }}
@endsection
 
@section('content')

<div class="container">
    <div class="breadcrumbs-w3l">
        <span class="breadcrumbs">
            <a href="{{ route('_root_') }}">Home</a> |
            @if($page->parent)
            <a href="{{ $page->parent->url }}">{{ $page->parent->title }}</a> |
            @endif
            <span>{{ $page->title }}</span>
        </span>
    </div>
</div>
<!-- inner-banner-bottom -->
<div class="welcome-wrap">
    <div class="container">
         <form action="#" method="POST">
                    {{ csrf_field() }}
        <div class="row">
            <div class="col-md-3">
               
                    <div class="form-group">
                        <label class="control-label">Country</label>
                        <select name="country" id="country" class="form-control dynamic" data-dependent="location">
                            <option value="">---- SELECT ----</option>
                        @foreach ($countries as $country)
                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                        @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Location</label>
                        <select name="location" id="location" class="form-control dynamic dynamical" data-dependent="university">
                            <option value="">Select Location</option>
                        </select>
                    </div>
                
            </div>

   
        </div>
        <div class="row">
            <div class="col-md-4">
                 <div id="university">
                     
                 </div>
            </div>
        </div>
    </form>
    </div>
</div>

@endsection

@section('scripts')
<script>
        $(document).ready(function(){

            $('.dynamic').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    console.log(select);
                    console.log(value);
                    console.log(dependent);
                    $.ajax({
                        url:"{{ route('dynamindependent.fetch_location') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {

                            $('#'+dependent).html(result);
                        }

                    })
                }
            });

            $('#country').change(function(){
                $('#location').val('');
            });



        });
    </script>
    <script>
        $(document).ready(function(){

            $('.dynamical').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    console.log(select);
                    console.log(value);
                    console.log(dependent);
                    $.ajax({
                        url:"{{ route('dynamindependent.fetch_university') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {

                            $('#'+dependent).html(result);
                        }

                    })
                }
            });

            $('#location').change(function(){
                $('#university').val('');
            });



        });
    </script>
 @endsection   