@component('mail::message')
 Thank You ** {{$request->name}}** for requesting for the service ** {{$request->service}} ** <br>
You will be contacted soon. 



Thanks,<br>
{{ config('app.name') }}
@endcomponent
