@component('mail::message')
# Respected {{$counselor->name}},

Your appointment has been scheduled as:-<br>

**Date-**{{$booking->a_date}}<br>
**Time-**{{$booking->time}}<br>

The details of the appointee is as follows:-<br>
**Name-**{{$booking->name}}<br>
**Email-**{{$booking->email}}<br>
**Phone-**{{$booking->phone}}<br>
**Address-**{{$booking->address}}



Thanks,<br>
{{ config('app.name') }}
@endcomponent
