@component('mail::message')
# Dear {{$booking->name}},

Your request for the appointment has been approved.



Thanks,<br>
{{ config('app.name') }}
@endcomponent
