@component('mail::message')

# Dear {{ $subscriber->name }},

New Service has been added. The details follows as:-
<br>
**Service Name** : {{$request->title}}<br>
**Details** : {!! $request->details !!}<br>
<br>
For further, Please feel free to call us.

{{ \App\Label::ofValue('global:tel_no') }}, {{ \App\Label::ofValue('global:mobile_no') }}

<br>
{{config('app.url')}}
<br>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
