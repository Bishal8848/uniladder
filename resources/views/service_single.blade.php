@extends('layouts.app')
@section('title')
    {{ $service->title }}
@stop
@section('content')
    <div class="container">
        <div class="breadcrumbs-w3l">
        <span class="breadcrumbs">
            <a href="{{ route('_root_') }}">Home</a> |

            <span>{{ucfirst($service->title)}}</span>
        </span>
        </div>
    </div>

    <div class="welcome-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading-text">
                        {{ $service->title }}
                    </h2>
                    @if($service->image)
                        <img src="{{asset('uploads/service/'.$service->image)}}" alt="{{$service->title}}" width="400" class="img-responsive pull-right" style="margin-left: 20px;">
                    @endif
                    {!! $service->details !!}
                </div>


            </div>
<br>
            <br>
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center">Latest services</h3>
                    <br>
                    <span class="header_button_span"></span>
                </div>
                @foreach($latest_services as $latest)
                <div class="col-md-4">
                    <div class="banner-subg1">
                        @if($latest->image)
                            <img src="{{asset('uploads/service/'.$latest->image)}}" alt="{{$latest->title}}" width="400" class="img-responsive pull-right" style="margin-left: 20px;">
                        @endif
                        <h3> {{ $latest->title }}</h3>
                        <p>{!! str_limit($latest->details, 180)  !!} </p>
                        <span class="fa fa-{{$latest->icon}}" aria-hidden="true"></span>
                        <div class="read-btn">
                            <a href="{{ route('viewservice', $latest->id) }}">view more</a>
                        </div>
                    </div>
                </div>



                @endforeach
            </div>
        </div>
    </div>


@stop
@section('script')
@stop
