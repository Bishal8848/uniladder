@extends('admin.app')

@section('title')
    Appointments
@endsection

@section('content')

    <div class="content-table">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Appointment Date</th>
                <th>Appointment Time</th>
                <th>Approval</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($bookings as $booking)
                <tr>
                    <td>{{$booking->name}}</td>
                    <td>{{$booking->email}}</td>
                    <td>{{$booking->phone}}</td>
                    <td>{{$booking->address}}</td>
                    <td>{{$booking->a_date}}</td>
                    <td>{{$booking->time}}</td>
                    <td>
                        @if($booking->status == 0)
                            <div class="pull-left" style="margin-right: 20px;">
                                <form class="form-group" action="{{ route('appointment.approve',encrypt($booking->id)) }}" method="post">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-success"><i class="lnr lnr-checkmark-circle"></i></button>
                                </form>
                            </div>

                            <div class="pull-left" style="margin-right: 20px;">
                                <form class="form-group" action="{{ route('appointment.decline',encrypt($booking->id)) }}" method="post">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-warning"><i class="lnr lnr-cross-circle"></i></button>
                                </form>
                            </div>
                        @elseif($booking->status == 1)
                            <p class="text-success"><i class="lnr lnr-checkmark-circle"></i> Approved </p>
                        @else
                            <p class="text-danger"><i class="lnr lnr-cross-circle"></i> Declined </p>
                        @endif
                    </td>
                    <td class="text-right">

                        <div class="pull-right" style="margin-left: 10px;">
                            <form onsubmit="return confirm('Are you sure?')" action="" method="post">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection