@extends('admin.app')

@section('title')
Subscriptions
@endsection

@section('content')

	<h3 class="page-title">Subscribers<a href="{{ route('subscriptions.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="content-table">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($subscriptions as $subscription)
			<tr>
				<td>{{ $subscription->name }}</td>
				<td>{!!$subscription->email !!}</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('subscriptions.edit', $subscription->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('subscriptions.destroy', $subscription->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
</div>
@endsection