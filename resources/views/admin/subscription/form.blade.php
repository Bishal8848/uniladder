<div class="form-group">
	<label class="col-sm-2 control-label" for="name">Name</label>
	<div class="col-sm-8">
		<input class="col-md-4 form-control" placeholder="Name" name="name" value="{{ old('name', isset($subscription) ? $subscription->name : null) }}" type="text" id="name">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="email">Email</label>
	<div class="col-sm-8">
		<input class="col-md-4 form-control" placeholder="Email" name="email" value="{{ old('email', isset($subscription) ? $subscription->email : null) }}" type="email" id="email" {{ isset($subscription) ? 'readonly="readonly"' : null }}>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-3 col-sm-offset-2">
		<div class="custom-control custom-checkbox mr-sm-2">
			<input type="checkbox" class="custom-control-input" id="status" name="status" value="1" {{ old( 'status', isset($subscription) ?
			 $subscription->status : 1) == 1 ? 'checked=checked' : null }}>
			<label class="custom-control-label" for="status">Active</label>
		</div>
	</div>
</div>