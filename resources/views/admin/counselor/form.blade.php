<div class="form-group">
	<label class="col-sm-2 control-label" for="name">Counselor Name</label>
	<div class="col-sm-8">
		<textarea rows="2" class="form-control" placeholder="Name" name="name" id="name"></textarea>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="email">Email</label>
	<div class="col-sm-8">
		<textarea rows="2" class="form-control" placeholder="Email" name="email" id="email"></textarea>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="phone">Phone Number</label>
	<div class="col-sm-8">
		<textarea rows="2" class="form-control" placeholder="Phone Number" name="phone" id="phone"></textarea>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-8">
		<textarea rows="2" class="form-control" placeholder="Details" name="details" id="details"></textarea>
	</div>
</div>
