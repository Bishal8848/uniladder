@extends('admin.app')

@section('title')
Inquiry Info
@endsection

@section('content')

<div class="add-new">
	<a href="{{ route('counselor.create') }}" class="btn btn-primary"><i class="lnr lnr-plus-circle"></i> <span>Add New</span></a>
</div>

<div class="content-table">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Details</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($counselor as $counselors)
			<tr>
				<td>{{$counselors->name}}</td>
				<td>{{$counselors->details}}</td>
				<td class="text-right">
					<a class="btn btn-primary btn-sm" href="{{ route('counselor.edit', $counselors->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;">
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('counselor.destroy', $counselors->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>
				@endforeach
		</tbody>
	</table>
</div>
@endsection