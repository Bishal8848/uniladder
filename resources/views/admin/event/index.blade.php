@extends('admin.app')

@section('title')
Activities/Seminar
@endsection

@section('content')

	<h3 class="page-title">Activities/Seminar <a href="{{ route('events.create') }}" class="btn btn-primary pull-right"><i class="lnr lnr-plus-circle"></i> <span>Add New</span></a></h3>



<div class="content-table">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Title</th>
				<th>Venue</th>
				<th>Date</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($events as $event)			
			<tr>
				<td>{{ $event->title }}</td>
				<td>{{ $event->event_place }}</td>
				<td>{{ $event->event_date }}</td>
				<td>
					@if ($event->status)
						<a class="btn btn-success btn-sm"  href="{{ route('events.showhide', $event->id) }}"><i class="lnr lnr-checkmark-circle"></i></a>						@else
						<a class="btn btn-danger btn-sm"  href="{{ route('events.showhide', $event->id) }}"><i class="lnr lnr-cross-circle"></i></a>						@endif [{{ $event->status ? 'Active' : 'Draft' }}]
				</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('events.edit', $event->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('events.destroy', $event->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
</div>
@endsection