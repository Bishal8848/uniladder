@extends('admin.app')

@section('title')
Services
@endsection

@section('content')

<div class="add-new">
	<a href="{{ route('services.create') }}" class="btn btn-primary"><i class="lnr lnr-plus-circle"></i> <span>Add New</span></a>
</div>

<div class="content-table">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Title</th>
				<th>Details</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($services as $service)			
			<tr>
				<td>{{ $service->title }}</td>
				<td>{!! str_limit($service->details,50) !!}</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('services.edit', $service->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('services.destroy', $service->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
</div>
@endsection