<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Title</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Title" name="title" value="{{ old('title', isset($service) ? $service->title : null) }}" type="text" id="title">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="8" placeholder="Details" name="details" id="details">{{ old('details', isset($service) ? $service->details : null) }}</textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Image</label>
	<div class="col-sm-10">
		<input type="file" name="image" id="image">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="icon">Icon</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Icon" name="icon" value="{{ old('icon', isset($service) ? $service->icon : null) }}" type="text" id="icon">
	</div>
</div>