

<div class="form-group">
	<label class="col-sm-2 control-label" for="name">Name</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Name" name="name" value="{{ old('name', isset($contactinfo) ? $contactinfo->name : null) }}" type="text" id="name" >
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="email">Email</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Email" name="email" value="{{ old('email', isset($contactinfo) ? $contactinfo->email : null) }}" type="email" id="email" >
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="Address">Address</label>
	<div class="col-sm-8">
		<textarea rows="5" class="form-control" placeholder="Address" name="address" id="address">{{ old('address', isset($contactinfo) ? $contactinfo->address : null) }}</textarea>
	</div>
</div>



<div class="form-group">
	<label class="col-sm-2 control-label" for="phone">Phone</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Phone" name="phone" value="{{ old('phone', isset($contactinfo) ? $contactinfo->phone : null) }}" type="text" id="phone" >
	</div>
</div>

<div class="form-group sec-right">
	<label class="col-sm-2 control-label contact-form-text">Select Country</label>
	<div class="col-sm-6">
	<select name="country" class="form-control">
		<option value="">---- SELECT ----</option>
		@foreach ($countries as $country)
			<option value="{{ $country->id }}">{{ $country->name }}</option>
		@endforeach
	</select>
	</div>
</div>
<div class="form-group sec-right">
	<label class="col-sm-2 control-label contact-form-text">Select Services</label>
	<div class="col-sm-6">
	<select name="service" class="form-control">
		<option value="">--------- SELECT ---------</option>
		@foreach ($services as $service)
			<option value="{{ $service->title }}">{{ $service->title }}</option>
		@endforeach
	</select>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="message">Message</label>
	<div class="col-sm-8">
		<textarea rows="5" class="form-control" placeholder="Message" name="message" id="message">{{ old('message', isset($inquiryinfo) ? $inquiryinfo->message : null) }}</textarea>
	</div>
</div>