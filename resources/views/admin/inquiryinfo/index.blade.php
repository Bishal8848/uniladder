@extends('admin.app')

@section('title')
Inquiry Info
@endsection

@section('content')

<div class="add-new">
	<a href="{{ route('inquiryinfos.create') }}" class="btn btn-primary"><i class="lnr lnr-plus-circle"></i> <span>Add New</span></a>
</div>

<div class="content-table">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>SN</th>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Address</th>
				<th>Inquiry Date</th>
			</tr>
		</thead>
		<tbody>
		<?php $i=1; ?>
			@foreach ($inquiryinfos as $inquiryinfo)
			<tr>
				<td>{{$i}}</td>
				<td>{{ $inquiryinfo->name }}</td>
				<td>{{$inquiryinfo->email}}</td>
				<td>{{ $inquiryinfo->phone }}</td>
				<td>{{ $inquiryinfo->address }}</td>
				<td>{{ $inquiryinfo->created_at->format('M d, Y') }}</td>
				<td class="text-right">
					<a href="{{ route('inquiryinfos.show', $inquiryinfo->id) }}" class="btn btn-warning btn-xs"><i class="lnr lnr-eye"></i></a>
						<a class="btn btn-primary btn-sm" href="{{ route('inquiryinfos.edit', $inquiryinfo->id) }}"><i class="lnr lnr-pencil"></i></a>

						<div class="pull-right" style="margin-left: 10px;"> 
							<form onsubmit="return confirm('Are you sure?')" action="{{ route('inquiryinfos.destroy', $inquiryinfo->id) }}" method="post">
								{{ method_field('DELETE') }}
								{{ csrf_field() }}		
								<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
							</form>
						</div>
				</td>
			</tr>
				<?php $i++; ?>
			@endforeach
		</tbody>
	</table>
</div>
@endsection