@extends('admin.app')

@section('title')
Inquiry Info
@endsection

@section('content')
<h3 class="page-title">Inquiry Info of <b>{{ $inquiryinfo->name }}</b> <a href="{{ route('inquiryinfos.index') }}" class="btn btn-primary pull-right"><span><i class="lnr lnr-arrow-left"></i> Back</span></a></h3>

<div class="panel">
	<div class="panel-body">
		<div class="table">
			<div>
				<label>Name:</label>
				<p>{{ $inquiryinfo['name'] }}</p>
			</div>

			<div>
				<label>Email:</label>
				<p>{{ $inquiryinfo->email }}</p>
			</div>

			<div>
				<label>Phone No:</label>
				<p>{{ $inquiryinfo->phone }}</p>
			</div>

			<div>
				<label>Address:</label>
				<p>{{ $inquiryinfo->address }}</p>
			</div>

			<div>
				<label>Country:</label>
				<p>{{ $inquiryinfo->country->code }} - {{ $inquiryinfo->country->name}}</p>
			</div>

			<div>
				<label>Service Selected:</label>
				<p>{{ $inquiryinfo->service_title }}</p>
			</div>

			<div>
				<label>Message:</label>
				<p>{{ $inquiryinfo->message }}</p>
			</div>
		</div>
	</div>
</div>
@endsection