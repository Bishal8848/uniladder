<div class="form-group sec-right">
	<label class="col-sm-2 control-label contact-form-text">Select Counselor</label>
	<div class="col-sm-6">
		<select name="counselor_id" class="form-control">
			<option value="">---- SELECT ----</option>
			@foreach ($counselors as $counselor)
				<option value="{{ $counselor->id }}">{{ $counselor->name }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Time</label>
	<div class="col-sm-6">
		<div class="row">
			<div class="col-sm-4"><input type="time" name="time[]" id="image"></div>
		</div><br>
		<div class="row">
			<div class="col-sm-4"><input type="time" name="time[]" id="image"></div>
		</div><br>
		<div class="row">
			<div class="col-sm-4"><input type="time" name="time[]" id="image"></div>
		</div><br>
		<div class="row">
			<div class="col-sm-4"><input type="time" name="time[]" id="image"></div>
		</div><br>
	</div>
</div>

