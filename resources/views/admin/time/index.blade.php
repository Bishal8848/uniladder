@extends('admin.app')

@section('title')
Inquiry Info
@endsection

@section('content')

<div class="add-new">
	<a href="{{ route('times.create') }}" class="btn btn-primary"><i class="lnr lnr-plus-circle"></i> <span>Add New</span></a>
</div>

<div class="content-table">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Time</th>
				<th>Details</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td></td>
				<td></td>
				<td class="text-right">
					<a class="btn btn-primary btn-sm" href=""><i class="lnr lnr-pencil"></i></a>
				</td>
			</tr>
		</tbody>
	</table>
</div>
@endsection