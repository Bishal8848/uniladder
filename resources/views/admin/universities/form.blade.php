<div class="form-group sec-right">
	<label class="col-sm-2 control-label contact-form-text">Select Country</label>
	<div class="col-sm-6">
	<select name="country" class="form-control">
		<option value="">---- SELECT ----</option>
		@foreach ($countries as $country)
			<option value="{{ $country->id }}">{{ $country->name }}</option>
		@endforeach
	</select>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="location">Location</label>
	<div class="col-sm-8">
		<textarea rows="2" class="form-control" placeholder="Location" name="location" id="location"></textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="university">University Name</label>
	<div class="col-sm-8">
		<textarea rows="2" class="form-control" placeholder="University" name="university" id="university"></textarea>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-8">
		<textarea rows="2" class="form-control" placeholder="Details" name="details" id="details"></textarea>
	</div>
</div>
<div class="form-group sec-right">
	<label class="col-sm-2 control-label contact-form-text">Services</label>
	<div class="col-sm-6">
	<label>
		@foreach ($services as $service)
		<input type="checkbox" name="services[]" value="{{ $service->title }}">{{ $service->title }}&nbsp;&nbsp;
		@endforeach
	</label>
		
	
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="website">Website</label>
	<div class="col-sm-8">
		<textarea rows="2" class="form-control" placeholder="Website" name="website" id="website"></textarea>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="Image">Image</label>
	<div class="col-sm-8">
		<input type="file" name="image">
	</div>
</div>