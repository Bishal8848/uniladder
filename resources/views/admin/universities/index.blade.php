@extends('admin.app')

@section('title')
University
@endsection

@section('content')

<div class="add-new">
	<a href="{{ route('universities.create') }}" class="btn btn-primary"><i class="lnr lnr-plus-circle"></i> <span>Add New</span></a>
</div>

<div class="content-table">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>SN</th>
				<th>Country</th>
				<th>Location</th>
				<th>University Name</th>
				<th>Website</th>
				<th>Image</th>
				<th>Services</th>
			</tr>
		</thead>
		<tbody>
		
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				
			</tr>
				
		</tbody>
	</table>
</div>
@endsection